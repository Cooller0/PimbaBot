#!/usr/bin/python3.6
##########################
## CRIADOR DO CODIGO #####
## COOLLER ###############
## TELEGRAM: @MrCooller ##
##########################

import telepot
import requests
import emoji
from gtts import gTTS
from bs4 import BeautifulSoup
import json
from socket import gethostbyname
from config import TABELA
import time
from telepot.loop import MessageLoop
from socket import*
from random import randint
#
#
def INFO(MSG):
    content_type, chat_type, chat_id = telepot.glance(MSG)
    if MSG['text'] == '/info':
        code = f"""
Nome: {MSG['from']['first_name']}
Usuario: {MSG['from']['username']}
Id: {MSG['from']['id']}
        """
        bot.sendMessage(chat_id, f'*{code}*', parse_mode='Markdown')

    elif MSG['text'] == '/id':
        code = f'ID do grupo {chat_id}'
        bot.sendMessage(chat_id, f'{code}')

    elif MSG['text'] == '/regras':          
        code = """
------ Regras ------
# Proibido pornografia
# Proibido divulgação
# Respeite os membros e adms
# Proibido vendas que não seja dos adms do grupo
# Proibido flood
# Proibido racismo 
        """
        bot.sendMessage(chat_id, f'*{code}*', parse_mode='MarkDown')


    elif MSG['text'] == 'obrigado' or MSG['text'] == 'Obrigado' or MSG['text'] == 'vlw' or MSG['text'] == 'valeu':
        bot.sendMessage(chat_id, ':)')
            
    elif MSG['text'][0:5] == '/fala':
        fala = MSG['text'][6:]
        AUDIO = gTTS(f'{fala}', lang="pt")
        AUDIO.save('audio.ogg')
            #
        audio = open('audio.ogg', 'rb')
        bot.sendAudio(chat_id, audio)

    elif MSG['text'] == '/help' or MSG['text'] == '/help@pimba_bot':
        texto = '''
##################
#### Comandos ####
##################
# REGRAS PADRÕES DO BOT
        /regras
#
# VER AS INFORMAÇÕES DO USUARIO QUE SOLICITAR O COMANDO
        /myinfo
#
# MOSTRA O ID DO GRUPO
        /id
#
# TRANSFORMA TEXTO EM AUDIO
        /fala Gosto muito de sorvete
#
# O BOT DA PRINT EM SUA MENSAGEM
        /print Gosto muito de sorvete  
#
# COLETA INFORMAÇÕES DO SITE
        /host www.exemplo.com
        /ip 127.0.0.1
#
# MANDA UMA PAYLOAD DA VIVO
        /payload_vivo
#
# MOSTRA AS OPERADORAS QUE ESTÃO FUNCIONANDO
        /status
#
# GERA UMA CONTA SSH FREE DE 7 DIAS
        /conta_ssh
#
# Banir e desbanir
        /ban
        /desban'''
        bot.sendMessage(chat_id, f'*{texto}*', parse_mode='MarkDown')

    elif MSG['text'][0:6] == '/ban':  
        admins = bot.getChatAdministrators(chat_id)
        ListAdm = [adm['user']['id'] for adm in admins]
        reply = MSG['reply_to_message']['from']['id']
        user = MSG['reply_to_message']['from']['first_name']
        if MSG['reply_to_message']:
            if MSG['from']['id'] in ListAdm:
                bot.kickChatMember(chat_id, reply)
                bot.sendMessage(chat_id, f"*{MSG['from']['first_name']} baniu {user}*", parse_mode='MarkDown')
            else:
                bot.sendMessage(chat_id, 'Não você é um adm')
        else:
                bot.sendMessage(chat_id, 'Esse usuario não foi banido')

    elif MSG['text'][0:9] == '/desban':
        admins = bot.getChatAdministrators(chat_id)
        ListAdm = [adm['user']['id'] for adm in admins]
        reply = MSG['reply_to_message']['from']['id']
        n = MSG['reply_to_message']['from']['first_name']
        if MSG['reply_to_message']:
            if MSG['from']['id'] in ListAdm:
                bot.unbanChatMember(chat_id, reply)
                bot.sendMessage(chat_id, f"*{MSG['from']['first_name']} desbaniu {n}*", parse_mode='MarkDown')
            else:
                bot.sendMessage(chat_id, 'Não você é um adm')
        else:
                bot.sendMessage(chat_id, 'Esse usuario não existe ou não pertence a esse grupo')
                                      
    elif MSG['text'][0:6] == '/print':
        txt = MSG['text'][7:]
        bot.sendMessage(chat_id, f'{txt}')

    elif MSG['text'][0:5] == '/host':
        HOST_ = MSG['text'][6:]
        ip = gethostbyname(HOST_)
        URL = requests.get(f'https://api.shodan.io/shodan/host/{ip}?key={CHAVE_II}')
        JSON = json.loads(URL.text)
        texto_ = f'''
Region Code: {JSON['region_code']}
IP: {JSON['ip_str']}
area code: {JSON['area_code']}
Country Name: {JSON['country_name']}
postal code: {JSON['postal_code']}
Dma Code: {JSON['dma_code']}
Cuntry Code: {JSON['country_code']}
City: {JSON['city']}
Longitude: {JSON['longitude']}
Country code3: {JSON['country_code3']}
Latitude: {JSON['latitude']}
OS: {JSON['os']}
Ports: {JSON['ports']}
            '''
        bot.sendMessage(chat_id, f'*{texto_}*', parse_mode='Markdown')

    elif MSG['text'][0:3] == '/ip':
        ip = MSG['text'][4:]
        URL = requests.get(f'https://api.shodan.io/shodan/host/{ip}?key={CHAVE_II}')
        JSON = json.loads(URL.text)
        texto_ = f'''
Region Code: {JSON['region_code']}
IP: {JSON['ip_str']}
area code: {JSON['area_code']}
Country Name: {JSON['country_name']}
postal code: {JSON['postal_code']}
Dma Code: {JSON['dma_code']}
Cuntry Code: {JSON['country_code']}
City: {JSON['city']}
Longitude: {JSON['longitude']}
Country code3: {JSON['country_code3']}
Latitude: {JSON['latitude']}
OS: {JSON['os']}
Ports: {JSON['ports']}
        '''
        bot.sendMessage(chat_id, f'*{texto_}*', parse_mode='Markdown')

    elif MSG['text'][0:8] == '/connect':
        bot.sendMessage(chat_id, 'Comando processado com sucesso!')
        
    elif 'claro' in MSG['text'] or 'Claro' in MSG['text'] or 'CLARO' in MSG['text']:
        bot.sendMessage(chat_id, 'Configurações para operadora claro ainda não estão funcionando!')

    elif MSG['text'] == '/payload_vivo':
        bot.sendMessage(chat_id, '''
*>> Payloard da vivo*
CONNECT [host_port]GET navegue.vivo.ddivulga.com/pacote[protocol][lf][split]CONNECT navegue.vivo.ddivulga.com/pacote HTTP/1.1[lf][lf]
                ''', parse_mode='Markdown')

    elif MSG['text'] == '/conta_ssh':
        bot.sendMessage(chat_id, '''
*Servidor:* nl-3.serverip.co
*Usuario:* fastssh.com-cooller
*Senha:* supernova
*Proxy:* nl-3.serverip.co:80
*Porta SSH:* 443 ou 22
                ''', parse_mode='Markdown')
      
    elif MSG['text'] == '/start':
        bot.sendMessage(chat_id, 'E ai, tudo bem?')
        if MSG['text'] == 'sim':
            bot.sendMessage(chat_id, '''Que bom saber que você está bem :)
Para mais informações digite /help''')

        elif MSG['text'] == 'não':
            bot.sendMessage(chat_id, 'achei que você estava feliz, mas tudo bem :), digite /help')  
        else:
            bot.sendMessage(chat_id, 'digite /help para mais informações')

    elif MSG['text'] == '/status':
        bot.sendMessage(chat_id, '''
*Operadora* *VIVO* - *On*
*Operadora* *CLARO* - *Off*
*Operadora* *TIM*s- *On*
*Operadora* *OI* - *On*
        ''', parse_mode='Markdown')

    elif 'pimba' in MSG['text'] or 'Pimba' in MSG['text']:
        bot.sendMessage(chat_id, r'Gosta de uma pimbada, né seu safadinho(a) heuheuheuheu')

    elif content_type == 'new_chat_member' :
        if chat_type == 'group' or chat_type == 'supergroup' :
            us = MSG['new_chat_member']['first_name']
            mensagem = redis.get('bemvindo:' + str(chat_id)).replace('#nome', us)
            if mensagem != None :
                bot.sendMessage(chat_id, mensagem)
    elif MSG['text'] == '/del':
        admins = bot.getChatAdministrators(chat_id)
        ListAdm = [adm['user']['id'] for adm in admins]
        if MSG['from']['id'] in ListAdm:
               bot.deleteMessage(telepot.message_identifier(MSG['reply_to_message']))
               bot.sendMessage(chat_id, f"""A mensagem de *{MSG['reply_to_message']['from']['first_name']}* foi apagada
e ainda levou uma pimbada multipla""", parse_mode='Markdown')

    elif MSG['text'] == '/link':
        a = bot.exportChatInviteLink(chat_id)
        bot.sendMessage(chat_id, a)
    elif 'pimba' in MSG['text'].lower() or MSG['reply_to_message']['from']['username']=='pimba_bot':
            url = 'https://in.bot/mod_perl/bot_gateway.cgi'
            r = requests.post(url, dict(
            server='0.0.0.0:8085',
            charset='utf-8',
            pure='1',
            js='0',
            msg=MSG['text']))
            soup = BeautifulSoup(r.text, 'html.parser')
            mensagem = soup.get_text()
            fala = mensagem
            AUDIO = gTTS(f'{fala}', lang="pt")
            AUDIO.save('audio.ogg')
            audio = open('audio.ogg', 'rb')
            bot.sendAudio(chat_id, audio, reply_to_message_id=MSG['message_id'])
    elif MSG['text'] == '/dado':
            dd = randint(1,6)
            bot.sendMessage(chat_id, f"Gira {emoji.emojize(':game_die:')}")
            time.sleep(2.0)
            bot.sendMessage(chat_id, f"{emoji.emojize(':game_die:')}{dd}")
    elif MSG['text'][0:5] == '/scan':
        bot.sendMessage(chat_id, '*Aguarde...*', parse_mode='MarkDown', reply_to_message_id=MSG['message_id'])
        sock = socket(AF_INET, SOCK_STREAM)
        ip = MSG['text'].split()[1]
        port = MSG['text'].split()[2]
        a = sock.connect_ex((str(ip), int(port)))
        if a == 0:
            bot.sendMessage(chat_id, f'''*
-> {ip}  
Porta {port} aberta*''', parse_mode='MarkDown', reply_to_message_id=MSG['message_id'])
        else:
            bot.sendMessage(chat_id, f'*Porta {port} fechada*', parse_mode='MarkDown', reply_to_message_id=MSG['message_id'])

# CONEXÃO
JSON = json.loads(TABELA)
CHAVE_I = JSON.get('BOT_KEY')
CHAVE_II = JSON.get('SHODAN_KEY')
bot = telepot.Bot(CHAVE_I)
MessageLoop(bot, INFO).run_as_thread()
while 1:
    time.sleep(10)
